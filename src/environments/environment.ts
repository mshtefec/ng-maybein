// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyAfQ0RYVTJPkpPusqwx8EcC2yOsU2Nvbo8",
    authDomain: "ng-business-dd777.firebaseapp.com",
    databaseURL: "https://ng-business-dd777.firebaseio.com",
    projectId: "ng-business-dd777",
    storageBucket: "ng-business-dd777.appspot.com",
    messagingSenderId: "567910469818",
    appId: "1:567910469818:web:21415234c5f7b45c247283",
    measurementId: "G-57YTG4XPTD"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
