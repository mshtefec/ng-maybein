// CORE
import { NgModule }                   from '@angular/core';
import { FormsModule }                from '@angular/forms';
import { HttpClientModule }           from '@angular/common/http';
import { BrowserModule }              from '@angular/platform-browser';
import { BrowserAnimationsModule }    from '@angular/platform-browser/animations';

// FIREBASE
import { AngularFireModule }          from '@angular/fire';
import { AngularFireAuth }            from '@angular/fire/auth';
import { AngularFireDatabaseModule }  from '@angular/fire/database';
import { AngularFirestore }           from '@angular/fire/firestore';
import { AngularFireStorageModule }   from '@angular/fire/storage';

// TERCEROS
import { ToastrModule }               from 'ngx-toastr';

// MODULOS
import { FrontModule }                from './components/front/front.module';
import { BackModule }                 from './components/backoffice/back.module';

// RUTAS
import { AppRoutingModule }           from './app-routing.module';

// COMPONENTES
import { AppComponent }               from './app.component';
import { LoginComponent }             from './components/login/login.component';
import { RegisterComponent }          from './components/register/register.component';

// SERVICIOS
import { AuthService }                from './services/auth.service';
import { AuthGuard }                  from './guards/auth.guard';

// VARIABLES
import { environment }                from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireStorageModule,

    ToastrModule.forRoot(),

    FrontModule,
    BackModule,
    AppRoutingModule
  ],
  providers: [
    AuthService, 
    AuthGuard, 
    AngularFireAuth, 
    AngularFirestore
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
