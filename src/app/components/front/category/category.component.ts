import { Component, OnInit }              from '@angular/core';

import { Product }                        from '@interfaces/product';
import { Category }                       from '@interfaces/category';
import { CrudService }                    from '@services/crud.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  title: string;
  url: string = 'https://firebasestorage.googleapis.com/v0/b/ng-business-dd777.appspot.com/o/images%2F';
  token: string = '?alt=media&token=7aa629fd-dba1-4abc-8611-5d999c3c157a';

  list: Product[];
  categories: Category[];

  constructor(
    private crud: CrudService
  ) { }

  ngOnInit() {

    this.title = "Joyas";

    this.crud.getAllByCollection('subcategories').subscribe(res => 
      this.categories = res
    );
    
    this.crud.getAllByCollection('products').subscribe(res => 
      this.list = res
    );
  }

  onFilter(filter?: string) {
    if (filter) {
      this.title = filter;
      this.crud.getAllByCollectionAndQuery('products', 'subcategory', filter).subscribe(res => 
        this.list = res
      );
    } else {
      this.title = "Joyas";
      this.crud.getAllByCollection('products').subscribe(res => 
        this.list = res
      );
    }
    
  }

}
