import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontRoutingModule } from './front-routing.module';

import { HomeComponent } from './home/home.component';
import { FrontComponent } from './front.component';
import { CategoryComponent } from './category/category.component';
import { NavbarComponent } from './ui/navbar/navbar.component';
import { FooterComponent } from './ui/footer/footer.component';

@NgModule({
  declarations: [HomeComponent, FrontComponent, CategoryComponent, NavbarComponent, FooterComponent],
  imports: [
    CommonModule,
    FrontRoutingModule
  ]
})
export class FrontModule { }
