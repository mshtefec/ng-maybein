import { NgModule }               from '@angular/core';
import { RouterModule, Routes }   from '@angular/router';

import { DemoComponent }          from './ui/demo/demo.component';
import { BlankComponent }         from './ui/blank/blank.component';
import { BackComponent }          from './back.component';

import { UserListComponent }      from './users/user-list/user-list.component';
import { UserFormComponent }      from './users/user-form/user-form.component';

import { CategoryListComponent }  from './categories/category-list/category-list.component';
import { CategoryFormComponent }  from './categories/category-form/category-form.component';

import { SubcategoryListComponent }  from './categories/subcategories/subcategory-list/subcategory-list.component';
import { SubcategoryFormComponent }  from './categories/subcategories/subcategory-form/subcategory-form.component';

import { ProductListComponent }   from './products/product-list/product-list.component';
import { ProductFormComponent }   from './products/product-form/product-form.component';

import { AuthGuard }              from 'src/app/guards/auth.guard';


const routes: Routes = [
  { 
    path: 'admin', 
    component: BackComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', component: BlankComponent },
      { path: '', redirectTo: 'admin', pathMatch: 'full' },
      { path: 'demo', component: DemoComponent },
      { path: 'products', component: ProductListComponent },
      { path: 'products/new', component: ProductFormComponent },
      { path: 'users', component: UserListComponent },
      { path: 'users/new', component: UserFormComponent },
      { path: 'categories', component: CategoryListComponent },
      { path: 'categories/new', component: CategoryFormComponent },
      { path: 'subcategories', component: SubcategoryListComponent },
      { path: 'subcategories/new', component: SubcategoryFormComponent },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackRoutingModule { }