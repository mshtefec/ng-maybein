import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DataTablesModule } from 'angular-datatables';

import { BackComponent } from './back.component';
import { BackRoutingModule } from './back-routing.module';

import { SidebarComponent } from './ui/sidebar/sidebar.component';
import { NavbarComponent } from './ui/navbar/navbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { BlankComponent } from './ui/blank/blank.component';
import { DemoComponent } from './ui/demo/demo.component';

import { RoleFormComponent } from './roles/role-form/role-form.component';
import { RoleListComponent } from './roles/role-list/role-list.component';
import { ProductFormComponent } from './products/product-form/product-form.component';
import { ProductFormEditComponent } from './products/product-form-edit/product-form-edit.component';
import { ProductListComponent } from './products/product-list/product-list.component';
import { SaleFormComponent } from './sales/sale-form/sale-form.component';
import { SaleListComponent } from './sales/sale-list/sale-list.component';

import { ModalComponent } from '../shared/modal/modal.component';

import { DemoMaterialModule } from './material.module';
import { UserFormComponent } from './users/user-form/user-form.component';
import { UserListComponent } from './users/user-list/user-list.component';
import { CategoryListComponent } from './categories/category-list/category-list.component';
import { CategoryFormComponent } from './categories/category-form/category-form.component';
import { SubcategoryFormComponent } from './categories/subcategories/subcategory-form/subcategory-form.component';
import { SubcategoryListComponent } from './categories/subcategories/subcategory-list/subcategory-list.component';

@NgModule({
    imports: [
      DataTablesModule,
      CommonModule,
      BackRoutingModule,
      FormsModule,
      ReactiveFormsModule,
      DemoMaterialModule
    ],
    declarations: [      
      BackComponent,
      BlankComponent,
      DemoComponent,
      SidebarComponent,
      NavbarComponent,
      FooterComponent,
      RoleFormComponent,
      RoleListComponent,
      ProductFormComponent,
      ProductFormEditComponent,
      ProductListComponent,
      SaleFormComponent,
      SaleListComponent,
      ModalComponent,
      UserFormComponent,
      UserListComponent,
      CategoryListComponent,
      CategoryFormComponent,
      SubcategoryListComponent,
      SubcategoryFormComponent
    ],
    entryComponents: [ModalComponent]
  })
  export class BackModule { }