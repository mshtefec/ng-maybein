import { Component, OnInit }    from '@angular/core';
import { NgForm }               from '@angular/forms';
import { Router }               from '@angular/router';

import { Category }             from '@interfaces/category';
import { CrudService }          from '@services/crud.service';

import { ToastrService }        from 'ngx-toastr';

@Component({
  selector: 'app-category-form',
  templateUrl: './category-form.component.html',
  styleUrls: ['./category-form.component.scss']
})
export class CategoryFormComponent implements OnInit {

  private formData: Category;

  constructor(
    private crud: CrudService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
    this.crud.initConfig('categories');
    this.resetForm();
  }

  onSubmit(form: NgForm) {
    
    if (form.value.id == null)
      this.insertRecord(form);
      //this.service.insert(form.value);
    // else
    //   this.updateRecord(form);
  }

  insertRecord(form: NgForm) {

    this.crud.createObjectAndSetId(form.value).then(() => {
      
      this.toastr.success('Cambios guardados', 'Elemento Registrado');
      this.resetForm(form);
      this.router.navigate(['admin/categories']);

    }, (error) => {
      console.error(error);
    });

  }

  resetForm(form?: NgForm) {
    if(form != null){
      form.resetForm();
    }
  }

}
