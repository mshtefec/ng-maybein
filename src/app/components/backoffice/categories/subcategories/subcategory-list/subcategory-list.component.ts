import { Component, ViewChild, OnInit }   from '@angular/core';

import Swal                               from 'sweetalert2';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { Product }                        from '@interfaces/product';
import { CrudService }                    from '@services/crud.service';

import { ModalComponent }                 from './../../../../shared/modal/modal.component';

@Component({
  selector: 'app-subcategory-list',
  templateUrl: './subcategory-list.component.html',
  styleUrls: ['./subcategory-list.component.scss']
})
export class SubcategoryListComponent implements OnInit {

  private title: string = "Categorias";
  private button: string = "Nueva Categoria";

  displayedColumns: string[] = ['id', 'name', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private crud: CrudService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    this.crud.initConfig('subcategories');
    
    this.crud.getAll().subscribe(res => 
      this.dataSource.data = res
    );

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew(data: Product) {
    this.openDialog();
  }

  onEdit(data: Product) {
    this.openDialog(data);
  }

  onDeleted(data: Product) {
    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este producto no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {
        
        this.crud.deletedObject(data).then(() => {
          Swal.fire('Eliminado!', 'Producto eliminado correctamente', 'success');
        }).catch((error) => {
          Swal.fire('Error!', 'No se puede eliminar el Producto', 'error');
        });

      }
    });
  }

  openDialog(data?: Product) {

    const config = {
      data: {
        message: data ? 'Editando Producto' : 'Nuevo Producto',
        content: data
      }
    }

    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    }); 
  }

}
