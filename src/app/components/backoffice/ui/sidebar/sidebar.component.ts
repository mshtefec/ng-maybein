import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  menues = [
    {name: "Usuarios", path: "users", icon: "user"},
    {name: "Categorias", path: "categories", icon: "chart-area"},
    {name: "Sub Categorias", path: "subcategories", icon: "chart-area"},
    {name: "Productos", path: "products", icon: "table"}
  ];

  constructor(
    private router: Router,
    private authService: AuthService, 
    private afsAuth: AngularFireAuth
  ) { }
  
  public isAdmin: any = null;
  public isChief: any = null;
  public isTrader: any = null;
  public isTechnical: any = null;

  public userUid: string = null;

  ngOnInit() {
    this.getCurrentUser();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserRoles(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('admin');
          this.isChief = Object.assign({}, userRole.roles).hasOwnProperty('chief');
          this.isTrader = Object.assign({}, userRole.roles).hasOwnProperty('trader');
          this.isTechnical = Object.assign({}, userRole.roles).hasOwnProperty('technical');
        })
      }
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

}
