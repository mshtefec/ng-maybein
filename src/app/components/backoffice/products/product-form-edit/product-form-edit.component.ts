import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { Product } from './../../../../interfaces/product';
import { ProductService } from 'src/app/services/product.service';

import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-product-form-edit',
  templateUrl: './product-form-edit.component.html',
  styleUrls: ['./product-form-edit.component.scss']
})
export class ProductFormEditComponent implements OnInit {

  @Input() product: Product;

  private image: any;
  private imageOriginal: any;

  constructor(
    private service: ProductService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
    this.image = this.product.image_url ? this.product.image_url : '';
    this.imageOriginal = this.product.image_url ? this.product.image_url : '';
  }

  onSubmit(form: NgForm) {
    this.updateRecord(form);
  }

  updateRecord(form: NgForm) {

    if (this.image == this.imageOriginal) {
      
      this.service.updateProduct(this.product.id, form.value).then(() => {
      
        this.toastr.info('Cambios guardados', 'Producto Actualizado');
        this.router.navigate(['admin/products']);
  
      }, (error) => {
        console.error(error);
      });
      
    } else {
      this.service.preAddAndUpdateProduct(form.value, this.image);
      
      this.service.updateProduct(this.product.id, form.value).then(() => {
      
        this.toastr.info('Cambios guardados', 'Producto Actualizado');
        this.router.navigate(['admin/products']);
  
      }, (error) => {
        console.error(error);
      });
    }

  }

  handleImage(event: any) {
    this.image = event.target.files[0];
  }

}
