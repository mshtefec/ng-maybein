import { Component, ViewChild, OnInit }   from '@angular/core';

import Swal                               from 'sweetalert2';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';
import { MatDialog }                      from '@angular/material/dialog';

import { Product }                        from '@interfaces/product';
import { CrudService }                    from '@services/crud.service';

import { ModalComponent }                 from './../../../shared/modal/modal.component';
import { AuthService } from '@app/services/auth.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  public id_user: string;

  displayedColumns: string[] = ['code', 'category', 'name', 'price_cost', 'price_sell', 'stock', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    private crud: CrudService,
    public dialog: MatDialog,
    private authService: AuthService,
  ) { }

  ngOnInit() {
    this.crud.initConfig('products');
    
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.authService.userAtributes(auth.uid).subscribe(atrs => {
          this.id_user = atrs.id;

          this.crud.getAllByIdUser(this.id_user).subscribe(res => 
            this.dataSource.data = res
          );

        })
      }
    });

  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew(data: Product) {
    this.openDialog();
  }

  onEdit(data: Product) {
    this.openDialog(data);
  }

  onDeleted(data: Product) {
    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este producto no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {
        
        this.crud.deletedObject(data).then(() => {
          Swal.fire('Eliminado!', 'Producto eliminado correctamente', 'success');
        }).catch((error) => {
          Swal.fire('Error!', 'No se puede eliminar el Producto', 'error');
        });

      }
    });
  }

  openDialog(data?: Product) {

    const config = {
      data: {
        message: data ? 'Editando Producto' : 'Nuevo Producto',
        content: data
      }
    }

    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    }); 
  }

}