import { Component, OnInit }    from '@angular/core';
import { NgForm }               from '@angular/forms';
import { Router }               from '@angular/router';

import { AngularFireStorage }   from '@angular/fire/storage';

import { Product }              from '@interfaces/product';
import { Category }             from '@interfaces/category';
import { Subcategory }          from '@interfaces/subcategory';

import { AuthService }          from '@services/auth.service';
import { CrudService }          from '@services/crud.service';

import { Observable }           from 'rxjs';
import { finalize }             from 'rxjs/operators';
import { ToastrService }        from 'ngx-toastr';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {

  private formData: Product;
  private id_user: string;
  private categories_list: Category[];
  private subcategories_list: Subcategory[];
  private image: any;

  private uploadPercent: Observable<number>;
  private urlImage: Observable<string>;
  private imageName: string = '';

  constructor(
    private crud: CrudService,
    private toastr: ToastrService,
    private authService: AuthService,
    private storage: AngularFireStorage,
    private router: Router
  ) { }

  ngOnInit() {
    this.crud.initConfig('products');
    this.crud.getAllByCollection('categories').subscribe(res => 
      this.categories_list = res
    );
    this.crud.getAllByCollection('subcategories').subscribe(res => 
      this.subcategories_list = res
    );
    this.getCurrentUser();
    this.resetForm();
  }

  onSubmit(form: NgForm) {
    
    if (form.value.id == null)
      this.insertRecord(form);
      //this.service.insert(form.value);
    // else
    //   this.updateRecord(form);
  }

  insertRecord(form: NgForm) {

    form.value.id_user = this.id_user;
    form.value.image_name = this.image.name;

    this.crud.createObjectAndSetId(form.value).then(() => {
      
      this.toastr.success('Cambios guardados', 'Elemento Registrado');
      this.resetForm(form);
      this.router.navigate(['admin/products']);

    }, (error) => {
      console.error(error);
    });

  }

  handleImage(event: any) {
    const file = event.target.files[0];
    this.image = file;
    const filePath = `images/${file.name}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe(
      finalize(
        () => this.urlImage = ref.getDownloadURL()
      )
    ).subscribe();
  }

  resetForm(form?: NgForm) {
    if(form != null){
      form.resetForm();
    }
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.authService.userAtributes(auth.uid).subscribe(atrs => {
          this.id_user = atrs.id;
        })
      }
    });
  }

}