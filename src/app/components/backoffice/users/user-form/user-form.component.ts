import { Component, OnInit }    from '@angular/core';
import { NgForm }               from '@angular/forms';
import { Router }               from '@angular/router';

import { CrudService }          from '@services/crud.service';
import { User }                 from '@interfaces/user';

import { ToastrService }        from 'ngx-toastr';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  private formData: User;
  private image: any;

  constructor(
    private crud: CrudService,
    private toastr: ToastrService,
    public router: Router
  ) { }

  ngOnInit() {
    this.crud.initConfig('users');
    this.resetForm();
  }

  onSubmit(form: NgForm) {
    // console.log(form.value);
    
    if (form.value.id == null)
      this.insertRecord(form);
      //this.service.insert(form.value);
    // else
    //   this.updateRecord(form);
  }

  insertRecord(form: NgForm) {

    // this.service.preAddAndUpdateProduct(form.value, this.image);
    
    this.crud.createObjectAndSetId(form.value).then(() => {
      
      this.toastr.success('Cambios guardados', 'Producto Registrado');
      this.resetForm(form);
      this.router.navigate(['admin/users']);

    }, (error) => {
      console.error(error);
    });

  }

  handleImage(event: any) {
    this.image = event.target.files[0];
  }

  resetForm(form?: NgForm) {
    if(form != null){
      form.resetForm();
    }
  }

}
