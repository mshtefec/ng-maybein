import { Component, ViewChild, OnInit }   from '@angular/core';

//material
import { MatPaginator }                   from '@angular/material/paginator';
import { MatSort }                        from '@angular/material/sort';
import { MatTableDataSource }             from '@angular/material/table';

import { CrudService }                    from '@services/crud.service';
import { User }                        from '@interfaces/user';

import Swal                               from 'sweetalert2';
import { MatDialog }                      from '@angular/material/dialog';
import { ModalComponent }                 from './../../../shared/modal/modal.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'username', 'email', 'actions'];
  dataSource = new MatTableDataSource();

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: false}) sort: MatSort;

  constructor(
    public dialog: MatDialog,
    private crud: CrudService
  ) { }

  ngOnInit() {
    this.crud.initConfig('users');

    this.crud.getAll().subscribe(res => 
      this.dataSource.data = res
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onNew(data: User) {
    this.openDialog();
  }

  onEdit(product: User) {
    // this.openDialog(product);
  }

  onDeleted(data: User) {
    Swal.fire({

      title: 'estas seguro?',
      text: 'al eliminar este elemento no se puede revertir!',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'

    }).then(result => {

      if (result.value) {
        
        this.crud.deletedObject(data).then(() => {
          Swal.fire('Eliminado!', 'Elemento eliminado correctamente', 'success');
        }).catch((error) => {
          Swal.fire('Error!', 'No se puede eliminar este elemento', 'error');
        });

      }
    });
  }

  openDialog(product?: User) {

    const config = {
      data: {
        message: product ? 'Editando Producto' : 'Nuevo Producto',
        content: product
      }
    }

    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    }); 
  }

}
