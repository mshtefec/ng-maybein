import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public email: string;
  public password: string;

  constructor(
      public authService: AuthService,
      public router: Router,
      private toastrService: ToastrService,
  ){}

  ngOnInit() {
  }

  onSubmitLogin() {
      this.authService.loginEmail(this.email, this.password)
      .then( (res) => {
          this.toastrService.success('Exito!', 'Usuario logueado!');
          this.router.navigate(['admin']);
      }).catch((err) => {
          this.toastrService.error('Error', 'Problemas de autenticación!', {
          progressBar: true
          });
          this.router.navigate(['login']);
      });
  }

}
