import { Injectable }           from '@angular/core';
import { AngularFireStorage }   from '@angular/fire/storage';
import { AngularFirestore }     from 'angularfire2/firestore';

import { Observable }           from 'rxjs';
import { map, finalize }        from 'rxjs/operators';

import { Product }              from '@interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  private filePath: any;
  private downloadURL: Observable<string>;

  private path: string;

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage,
  ) { }

  initConfig(path: string) {
    this.path = path;
  }

  getAll() {
    return this.afs.collection(this.path).valueChanges();
  }

  getAllByCollection(colection: string) {
    return this.afs.collection(colection).valueChanges();
  }

  getAllByIdUser(idUser: string) {
    return this.afs.collection(this.path, ref => ref.where('id_user', '==', idUser)).valueChanges();
  }

  getAllByCollectionAndQuery(colection: string, atribute: string, condition: string) {
    return this.afs.collection(colection, ref => ref.where(atribute, '==', condition)).valueChanges();
  }

  // devuelve los productos pero con el id asociado a cada documento en la colección.
  getProductsWithId(): Observable<Product[]> {
    return this.afs
      .collection('products')
      .snapshotChanges()
      .pipe(
        map(actions => 
          actions.map(a => {
            const data = a.payload.doc.data() as Product;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  getObject(id: string) {
    return this.afs.collection(this.path).doc(id).snapshotChanges();
  }

  createObject(data: any) {
    return this.afs.collection(this.path).add(data);
  }

  // obtengo la id que se va a crear en la colección dentro del objeto producto
  // actualizo el objeto seteando la id y guardo la colección pero con set() no add()
  // esta función permite optimizar el listado de productos
  createObjectAndSetId(data: any) {
    const id = this.afs.createId();
    data.id = id;
    return this.afs.collection(this.path).doc(id).set(data);
  }

  updateProduct(id: string, data: Product) {
    return this.afs.collection('products').doc(id).set(data);
  }

  deletedObject(data: any) {
    return this.afs.collection(this.path).doc(data.id).delete();
  }

  preAddAndUpdateObject(data: any, image: File) {
    this.filePath = `images/${this.path}/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(urlImage => {
            this.downloadURL = urlImage;
            
            data.image_post = urlImage;
            data.file_ref = this.filePath;
            
          });

          return data;
        })
      ).subscribe();
  }

  // private savePost(post: PostI) {
  //   const postObj = {
  //     titlePost: post.titlePost,
  //     contentPost: post.contentPost,
  //     imagePost: this.downloadURL,
  //     fileRef: this.filePath,
  //     tagsPost: post.tagsPost
  //   };

  //   if (post.id) {
  //     return this.postsCollection.doc(post.id).update(postObj);
  //   } else {
  //     return this.postsCollection.add(postObj);
  //   }

  // }

}
