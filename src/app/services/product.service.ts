import { Injectable } from '@angular/core';
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireStorage } from '@angular/fire/storage';

import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';

import { Product } from './../interfaces/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private filePath: any;
  private downloadURL: Observable<string>;

  constructor(
    private afs: AngularFirestore,
    private storage: AngularFireStorage
  ) { }

  getProducts() {
    return this.afs.collection('products').valueChanges();
  }

  // devuelve los productos pero con el id asociado a cada documento en la colección.
  getProductsWithId(): Observable<Product[]> {
    return this.afs
      .collection('products')
      .snapshotChanges()
      .pipe(
        map(actions => 
          actions.map(a => {
            const data = a.payload.doc.data() as Product;
            const id = a.payload.doc.id;
            return { id, ...data };
          })
        )
      );
  }

  getProduct(id: string) {
    return this.afs.collection('products').doc(id).snapshotChanges();
  }

  createProduct(data: Product) {
    return this.afs.collection('products').add(data);
  }

  // obtengo la id que se va a crear en la colección dentro del objeto producto
  // actualizo el objeto seteando la id y guardo la colección pero con set() no add()
  // esta función permite optimizar el listado de productos
  createProductAndSetId(data: Product) {
    const id = this.afs.createId();
    data.id = id;
    return this.afs.collection('products').doc(id).set(data);
  }

  updateProduct(id: string, data: Product) {
    return this.afs.collection('products').doc(id).set(data);
  }

  deletedProduct(data: Product) {
    return this.afs.collection('products').doc(data.id).delete();
  }

  preAddAndUpdateProduct(data: Product, image: File) {
    return this.uploadImage(data, image);
  }

  private uploadImage(data: Product, image: File) {
    // this.filePath = `images/products/${image.name}`;
    // const fileRef = this.storage.ref(this.filePath);
    // const task = this.storage.upload(this.filePath, image);
    // task.snapshotChanges()
    //   .pipe(
    //     finalize(() => {
    //       fileRef.getDownloadURL().subscribe(urlImage => {
    //         this.downloadURL = urlImage;
            
    //         data.image_url = this.downloadURL;
    //         data.file_ref = this.filePath;
            
    //       });
    //     })
    //   ).subscribe();
  }

  // private savePost(post: PostI) {
  //   const postObj = {
  //     titlePost: post.titlePost,
  //     contentPost: post.contentPost,
  //     imagePost: this.downloadURL,
  //     fileRef: this.filePath,
  //     tagsPost: post.tagsPost
  //   };

  //   if (post.id) {
  //     return this.postsCollection.doc(post.id).update(postObj);
  //   } else {
  //     return this.postsCollection.add(postObj);
  //   }

  // }

}
