export interface Product {
    id?: string;
    code?: string;
    name?: string;
    price_cost?: number;
    price_sell?: number;
    stock?: number;
    category?: string;
    subcategory?: string;
    image_url?: string;
    image_name?: string;
    id_user?: string;
}
